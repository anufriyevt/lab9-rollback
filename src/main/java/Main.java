import java.sql.*;

public class Main {
    static final String DB_URL = "jdbc:postgresql://localhost:5432/company";

    static final String USER = "username";
    static final String PASS = "password";

    public static void printRs(ResultSet rs) throws SQLException {
        rs.beforeFirst();
        while (rs.next()) {
            int id = rs.getInt("id");
            int age = rs.getInt("age");
            String first = rs.getString("first");
            String last = rs.getString("last");


            System.out.print("ID: " + id);
            System.out.print(", Age: " + age);
            System.out.print(", First: " + first);
            System.out.println(", Last: " + last);
        }
        System.out.println();
    }

    public static void rollback_func() {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            conn.setAutoCommit(false);

            System.out.println("Creating statement...");
            stmt = conn.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);

            System.out.println("Inserting one row....");
            String SQL = "INSERT INTO Employees " +
                    "VALUES (108, 20, 'Jane', 'Eyre')";

            stmt.executeUpdate(SQL);
            System.out.println("Commiting data here....");
            conn.commit();

            System.out.println("Inserting one row....");
            SQL = "INSERT INTO Employees " +
                    "VALUES (109, 20, 'David', 'Rochester')";

            stmt.executeUpdate(SQL);

            System.out.println("Inserting one row....");
            SQL = "INSERT INTO public.Employees " +
                    "VALUES (106, 20, 'Rita', 'Tez')";
            stmt.executeUpdate(SQL);
            SQL = "INSERT INTO public.Employees " +
                    "VALUES (107, 22, 'Sita', 'Singh')";
            stmt.executeUpdate(SQL);

            System.out.println("Commiting data here....");
            conn.commit();

            String sql = "SELECT id, first, last, age FROM public.Employees";
            ResultSet rs = stmt.executeQuery(sql);
            System.out.println("List result set for reference....");
            printRs(rs);
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
            System.out.println("Rolling back data here....");
            try {
                if (conn != null)
                    conn.rollback();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        System.out.println("Goodbye!");
    }

    public static void addSalaries(LinkedList<Integer> ids, LinkedList<Integer> employeeIds, LinkedList<Integer> amount) {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            conn.setAutoCommit(false);

            System.out.println("Creating statement...");
            stmt = conn.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);


            for (int i = 0; i < Math.max(ids.size(), Math.max(employeeIds.size(), amount.size())); i++) {
                System.out.println("Inserting one row....");
                SQL = "INSERT INTO public.salary " +
                        "VALUES (" + ids.get(i) + "," +
                        employeeIds.get(i) + "," +
                        amount.get(i) + ")";

                stmt.executeUpdate(SQL);
            }

            System.out.println("Commiting data here....");
            conn.commit();

            stmt.close();
            conn.close();
        } catch (SQLException | IndexOutOfBoundsException se) {
            se.printStackTrace();
            System.out.println("Rolling back data here....");
            try {
                if (conn != null)
                    conn.rollback();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        System.out.println("End");

    }
}

    public static void main(String[] args) {
        LinkedList<Integer> ids = new LinkedList<>();
        ids.add(200);
        ids.add(201);
        ids.add(202);

        LinkedList<Integer> employeeIds = new LinkedList<>();
        employeeIds.add(106);
        employeeIds.add(107);
        employeeIds.add(108);

        LinkedList<Integer> amount = new LinkedList<>();
        amount.add(3489);
        amount.add(90456);
        amount.add(34806);

        addSalaries(ids, employeeIds, amount);
    }
